
; -------------------- primitives --------------------------
(literal_number) @number
(literal_string) @string
(literal_boolean) @boolean


(type_identifier)  @type

(comment) @comment


(type) @type

[
 ":"
 (return_arrow)
 ","
 "."
] @punctuation.delimiter

[
 "("
 ")"
 "{"
 "}"
] @punctuation.bracket

[
 "-"
 "+"
 "/"
 "*"
 (assignment_operator)
 (greater_than_operator)
 (less_than_operator)
 (less_than_or_equal_to)
 (greater_than_or_equal_to)
 (logical_and)
 (logical_or)
 (logical_not)
] @operator

[
 (if_keyword)
 (else_keyword)
 (else_if_keywords)
] @conditional

"loop" @repeat

; --------------------- functions --------------------------
(function_keyword) @keyword.function

[
 (data_keyword)
 (variable_keyword)
 (new_keyword)
 (dereference_keyword)
 (break)
] @keyword

(function_definition
  name: (function_identifier) @function
)

(parameter_list
  parameter: (identifier) @variable @parameter
  (type)
)



(function_call
  name: (function_identifier) @function
)

(return_keyword) @keyword.return

(identifier) @variable
