module.exports = grammar({
  name: 'rexc',
  word: $ => $.identifier,
  rules: {
    source_file: $ => repeat($._block),
    _block: $ => choice(
        $._definition,
        $._expression,
        $._statement,
        $.comment,
        $.loop,
        $.return_keyword,
    ),
    _definition: $ => choice(
      $.function_definition,
      $.variable_definition,
      $.variable_assignment,
      $.struct_definition,
    ),
    _expression: $ => choice(
      $.binary_operation,
      $.function_call,
      $._literal_primitive,
      $.parenthesized_expression,
      $._variable,
      $.equality_expression,
      $.not_expression,
      $.struct_instantiation
    ),
    comment: $ => token(seq('#', /.*/)),
    function_definition: $ => seq(
        field('function', $.function_keyword),
        field('name', alias($.identifier, $.function_identifier)),
        field('parameters', $.parameter_list),
        optional(seq(
          $.return_arrow,
          field('return_type', $.type),
        )),
        $._accoladed_block,
    ),
    return_arrow: $ => '->',
    struct_definition: $ => seq(
      $.data_keyword,
      alias($.identifier, $.type_identifier),
      '{',
      repeat($.type_assignment),
      '}',
    ),
    struct_instantiation: $ => seq(
      $.new_keyword,
      alias($.identifier, $.type_identifier),
      '{',
      repeat(seq(
        $.variable_assignment,
        optional(',')
      )),
      '}'
    ),
    property_dot_notation: $ => seq(
      $.identifier,
      repeat1($._dot_accessor)
    ),
    _dot_accessor: $ => seq(
      '.',
      $.identifier
    ),
    function_call: $ => prec(1, seq( // prec = 1 bc parethesized_expression
        field('name', alias($.identifier, $.function_identifier)),
        '(',
        repeat(seq(
          field('argument', $._expression),
          optional(',')
        )),
        ')'
    )),
    parameter_list: $ => seq(
        '(',
        repeat(seq(
            field('parameter', $.identifier), // not reusing `type_assignment` bc want specific field name
            ':',
            $.type,
            optional(',')
        )),
        ')'
    ),
    type: $ => choice(
      alias($.identifier, $.type_identifier),
      $._pointer_type,
      $.lambda_type,
      'bool',
      'int',
      'function',
      'nil',
    ),
    _pointer_type: $ => seq(
      repeat1(field('pointer', '*')),
      alias($.identifier, $.type_identifier)
    ),
    _literal_primitive: $ => choice(
      $.literal_number,
      $.literal_string,
      $.literal_boolean,
    ),
    _statement: $ => choice(
      $.if_statement,
      $.variable_declaration,
    ),
    else_statement: $ => seq(
      $.else_keyword,
      $._accoladed_block
    ),
    else_if_statement: $ => seq(
      $.else_if_keywords,
      $.conditions,
      $._accoladed_block,
    ),
    conditions: $ => seq(
      $._expression,
      repeat(seq(
        $._logical_operator,
        $._expression,
      ))
    ),
    equality_expression: $ => prec.left(seq(
      $._expression,
      $._equality_operator,
      $._expression,
    )),
    not_expression: $ => prec.left(0, seq(
      $.logical_not,
      $._expression
    )),
    logical_not: $ => '!',
    _other_operators: $ => choice( // definitely no commonalities in operations here. no way broski
      '+',
      '-',
      '*',
      '/',
    ),
    _equality_operator: $ => choice(
      $.greater_than_operator,
      $.less_than_operator,
      $.equal_to_operator,
      $.less_than_or_equal_to,
      $.greater_than_or_equal_to
    ),
    greater_than_operator: $ => '>',
    less_than_operator: $ => '<',
    equal_to_operator: $ => '==',
    less_than_or_equal_to: $ => '<=',
    greater_than_or_equal_to: $ => '>=',
    logical_and: $ => '&&',
    logical_or: $ => '||',
    _logical_operator: $ => choice(
      $.logical_and,
      $.logical_or,
    ),
    _operator: $ => prec.left( choice(
      $._logical_operator,
      $._equality_operator,
      $._other_operators,
    )),
    binary_operation: $ => prec.left(2, seq(
      $._expression,
      $._operator,
      $._expression,
    )),
    _loop_block: $ => choice(
      $._block,
      $.break
    ),
    loop: $ => seq(
      'loop',
      '{',
      repeat($._loop_block),
      '}',
    ),
    break: $ => 'break',
    if_statement: $ => seq(
      $.if_keyword,
      $.conditions,
      $._accoladed_block,
      seq(
        repeat($.else_if_statement),
        optional($.else_statement)
      )
    ),
    else_if_keywords: $ => 'else if',
    if_keyword: $ => 'if',
    else_keyword: $ => 'else',
    new_keyword: $ => 'new',
    _accoladed_block: $ => seq(
      '{',
      repeat($._block),
      '}',
    ),
    parenthesized_expression: $ => prec.left(seq(
      '(',
      $._expression,
      ')'
    )),
    parenthesized_dot_expression: $ => prec(2, seq(
      $.parenthesized_expression,
      repeat($._dot_accessor)
    )),
    variable_definition: $ => seq(
      $.variable_declaration,
      $.assignment_operator,
      $._expression
    ),
    variable_declaration: $ => seq(
      $.variable_keyword,
      $.type_assignment
    ),
    variable_assignment: $ => seq(
      $._variable,
      $.assignment_operator,
      $._expression
    ),
    assignment_operator: $ => '=',
    keyword: $ => choice( // todo: is keyword wanted?
      $.function_keyword,
      $.return_keyword,
      $.variable_keyword
    ),
    function_keyword: $ => 'function',
    data_keyword: $ => 'data',
    variable_keyword: $ => 'var',
    return_keyword: $ => 'return',
    dereference_keyword: $ => 'at',
    literal_number: $ => /\d+/,
    literal_string: $ => /".*"/,
    literal_boolean: $ => choice(
      'true',
      'false'
    ),
    lambda_type: $ => seq(
      repeat(field('pointer', '*')),
      'func',
      '(',
      $.type_params,
      ')',
      $.return_arrow,
      field('return_type', $.type)
    ),
    type_params: $ => repeat1(seq(
      $.type,
      optional(',')
    )),
    type_assignment: $ => seq(
      field('variable_name', $.identifier),
      ':',
      $.type,
    ),
    dereference_expression: $ => prec.left(1, seq(
      repeat1($.dereference_keyword),
      $._variable
    )),
    // variable = one or more identifier(s)
    // which resolves to a single value
    _variable: $ => choice(
      $.dereference_expression,
      $.property_dot_notation,
      $.parenthesized_dot_expression,
      $.identifier,
    ),
    identifier: $ => /[a-zA-Z_]+[0-9a-zA-Z_]*/,
    type_identifier: $ => $.identifier, // for accurate highlights
    function_identifier: $ => $.identifier // for accurate highlights
  }
});

