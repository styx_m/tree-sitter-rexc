- [About](#about)
- [Running the project](#running-the-project)
- [Editor specific instructions](#editor-specific)
  - [Neovim](#neovim)
    - [Enable treesitter for rexc](#enable-treesitter)
    - [Enable highlights](#enable-highlights)

### About
A [treesitter](https://tree-sitter.github.io/tree-sitter/) parser for the [rexc](https://github.com/pixlark/rexc) language.

![Screenshot of a four-way split window of code highlighted via treesitter](https://gitlab.com/styx_m/tree-sitter-rexc/uploads/7f37c637d93043577514bbccc5acfe27/2023-04-10-15_37_53-fullscreen.png "Treesitter highlights in Neovim")

### Running the project
- `npm install` - Install dependencies
- `npm run build` - Builds project
- `npm test` - Build project and execute tests

#### Ensuring things work
- Run `tree-sitter highlight /path/to/file` should print some (ugly) highlights
  - Your editor will use its own colorscheme

### Editor-specific

#### NeoVim
##### Enable treesitter
```lua
local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.rexc = {
    install_info = {
        url = "/path/to/tree-sitter-rexc", -- local path or git repo
        files = {"src/parser.c"},
    },
    filetype = "rx", -- if filetype does not match the parser name
}
```

then:
- `:TSInstall rexc`
- `:set filetype=rx`

##### Enable highlights
- Add a directory containing `queries/rexc/highlights.scm` to your runtimepath
  - Tip: I created a new directory tree somewhere I don't care about and symlinked `highlights.scm` to this cloned repo

